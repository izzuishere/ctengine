# CT.cpp

## Compiling
The project uses both Meson & CMake for your convenience.


#### Recommended Compiling Instructions
### Linux
`make; make clean`

### OSX with Meson
`meson build; ninja -C "build" install`

### Windows
`something something meson`
